/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : constract_lock

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 22/02/2022 18:11:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for uploadfile
-- ----------------------------
DROP TABLE IF EXISTS `uploadfile`;
CREATE TABLE `uploadfile`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `size` double(20, 3) NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  `old_name` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  `create_time` date NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  `uuid_name` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of uploadfile
-- ----------------------------
INSERT INTO `uploadfile` VALUES (38, 11.000, '.txt', 'Test.txt', '2022-02-22', 'G:\\upload\\2022-02-22\\f905441e-4b71-473d-b716-cf2cade59971.txt', 'f905441e-4b71-473d-b716-cf2cade59971');
INSERT INTO `uploadfile` VALUES (39, 42744.000, '.jpg', 'keli.jpg', '2022-02-22', 'G:\\upload\\2022-02-22\\09048ab8-100d-4f9f-98a7-b97fae3c850d.jpg', '09048ab8-100d-4f9f-98a7-b97fae3c850d');

SET FOREIGN_KEY_CHECKS = 1;
