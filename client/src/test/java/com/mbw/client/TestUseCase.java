package com.mbw.client;

import com.mbw.client.useCase.Download;
import com.mbw.client.useCase.FileData;
import com.mbw.client.useCase.UploadFile;
import org.junit.Test;

public class TestUseCase {
    //测试上传功能
    @Test
    public void upload() throws Exception {
//        System.out.println( UploadFile.uploadFile("G:\\Test.txt"));
        System.out.println(UploadFile.uploadFile("G:\\keli.jpg"));
    }

    //测试下载文件
    @Test
    public void Download() throws Exception {
//        System.out.println(Download.downloadFile("f905441e-4b71-473d-b716-cf2cade59971"));
        System.out.println(Download.downloadFile("09048ab8-100d-4f9f-98a7-b97fae3c850d"));

    }

    //测试获取文件数据功能
    @Test
    public void fileData() throws Exception {
        System.out.println( FileData.getData("f905441e-4b71-473d-b716-cf2cade59971"));
    }
}
