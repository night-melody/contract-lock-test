package com.mbw.client.useCase;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mbw.client.util.HttpUtil;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;

public class FileData {

    public static JSONObject getData(String uuidName) throws Exception {
        HttpURLConnection connection = HttpUtil.creatUtil("http://127.0.0.1:9090/file/fileinfo?uuidName="+uuidName, "GET");
       InputStream is= connection.getInputStream();//以输入流的形式返回
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        byte [] buffer=new byte[1024];
        int len=0;
        while((len=is.read(buffer))!=-1){
            baos.write(buffer, 0, len);
        }
        String jsonString=baos.toString();
        baos.close();
        is.close();
        //转换成json数据处理
        return JSON.parseObject(jsonString);

    }
}
