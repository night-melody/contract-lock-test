package com.mbw.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UploadFile {
    private double size;
    private String type;
    private String oldName;
    private Date createTime;
    private String url;
    private String uuidName;
    private long id;

    public UploadFile(Double size, String type, String oldName, Date createTime, String url, String uuidName) {
        this.size = size;
        this.type = type;
        this.oldName = oldName;
        this.createTime=createTime;
        this.url = url;
        this.uuidName = uuidName;
    }
}
