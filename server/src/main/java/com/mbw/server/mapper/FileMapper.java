package com.mbw.server.mapper;

import com.mbw.server.pojo.UploadFile;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FileMapper {

    //增加文件
    int addFile(UploadFile uploadFile);

    //根据UUID获取文件
    UploadFile findFile(String uuidName);

}
