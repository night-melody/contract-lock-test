package com.mbw.server.service;


import com.mbw.server.mapper.FileMapper;
import com.mbw.server.pojo.UploadFile;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class FileServiceimpl implements FileService {

    @Resource
    FileMapper fileMapper;


    @Override
    public int add(UploadFile uploadFile) {
        fileMapper.addFile(uploadFile);
        return 0;
    }

    @Override
    public UploadFile findFileUrl(String uuidName) {
        return fileMapper.findFile(uuidName);
    }
}
