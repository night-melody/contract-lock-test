package com.mbw.server.service;


import com.mbw.server.pojo.UploadFile;

public interface FileService {

    int add(UploadFile uploadFile);
    UploadFile findFileUrl(String uuidName);
}
