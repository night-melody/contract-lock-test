package com.mbw.server.controller;

import com.mbw.server.pojo.UploadFile;
import com.mbw.server.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.sql.Date;
import java.util.UUID;

@RestController
@RequestMapping("/file")
@Slf4j
public class FileServer {
    @Resource
    FileService fileService;
    //上传文件
    @PostMapping(value = "/upload")
    public String FileUploadServer(HttpServletRequest request)  throws Exception {
        String uuid;
        uuid= UUID.randomUUID().toString();
        log.info("接收文件的UUID="+uuid);
        InputStream inStream = request.getInputStream();
        String dirPath = "G:\\upload\\";
        File file=new File(dirPath +"\\"+request.getHeader("time"));
        if(!file.exists()){
            file.mkdir();
          log.info("新建文件夹");
        }


        File newFile = new File( dirPath +request.getHeader("time")+"\\"+uuid+(request.getHeader("filetype")));
        FileOutputStream fos=new FileOutputStream(newFile);
        byte[] buffer = new byte[2048];
        int len;
        while ((len=inStream.read(buffer))!=-1){
            fos.write(buffer,0,len);
        }
        /*文件大小*/
        Double size=Double.valueOf(request.getHeader("fileSize"));

        /*文件类型*/
        String type=request.getHeader("fileType");

        /*原始文件名*/
        String oldName= URLDecoder.decode(request.getHeader("fileName"),"UTF-8");


        /*创建时间*/
        Date createTime=new Date(System.currentTimeMillis());
        /*文件保存目录*/
        String url= dirPath +request.getHeader("time")+"\\"+uuid+(request.getHeader("filetype"));
        /*文件UUID*/
        String uuidName=uuid;


        UploadFile uploadFile=new UploadFile(size,type,oldName,createTime,url,uuidName);
        log.info("上传文件详情"+uploadFile);

        fileService.add(uploadFile);
        inStream.close();
        fos.close();
        log.info("上传成功");
        return uuid;
    }

    /*下载Controller*/
    @GetMapping(value = "/download")
    public void FileDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UploadFile uploadfile = fileService.findFileUrl(request.getHeader("uuid"));
        System.out.println(uploadfile);
        if(uploadfile==null){
            response.setStatus(404);
            log.info("文件不存在");
            return;
        }
        /*定位数据源*/
        File f=new File(uploadfile.getUrl());
        /*建立管道*/
       InputStream in=new FileInputStream(f);
        OutputStream out=new DataOutputStream(response.getOutputStream());
        response.setHeader("fileType",uploadfile.getType());
        response.setHeader("uuid",uploadfile.getUuidName());
        /*操作管道*/
       byte[] buf=new byte[2048];
       int bytesRead;
       while ((bytesRead=in.read(buf))>0){
           out.write(buf,0,bytesRead);
       }
        in.close();
        out.close();
        log.info("文件下载成功");

    }

    /*获取文件元数据*/
    @GetMapping(value = "/fileinfo")
    public UploadFile getFileInfo(String uuidName) {
        return fileService.findFileUrl(uuidName);
    }
}
